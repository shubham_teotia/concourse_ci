#!/usr/bin/env bash

#set -x

# Set script ID
SCRIPT_ID=concourse.cbx-copy_app-task.debug
echo $APP_PATH
# Getting app jar name absolute path
jar_path=$(find / -name '$APP_NAME')
echo $jar_path
mkdir  -m 777 -p "$OUTPUT_DIR"
echo directory created
echo $(ls) 
# Copying app to task output folder
#cp  "$jar_path" "$dirname"
cp "$APP_PATH" "$OUTPUT_DIR"
cp "$DOCKER_FILE" "$OUTPUT_DIR" 
